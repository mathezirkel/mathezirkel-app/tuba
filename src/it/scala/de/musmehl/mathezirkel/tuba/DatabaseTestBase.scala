package de.musmehl.mathezirkel.tuba

import io.getquill._
import org.scalatest._
import org.scalatest.flatspec.AnyFlatSpecLike

import scala.io.Source
import scala.util.{Failure, Success, Using}

object DatabaseTestBase {
  val ctx = new PostgresJdbcContext(SnakeCase, "testDB")
}

trait DatabaseTestBase
    extends AnyFlatSpecLike
    with BeforeAndAfterAll
    with RandomTestOrder {
  implicit val ctx = DatabaseTestBase.ctx

  // Read and apply SQL file
  private def createSchema() = Using.Manager { use =>
    val schemaSql   = use(Source.fromResource("sql/schema.sql")).mkString
    val defaultsSql = use(Source.fromResource("sql/defaults.sql")).mkString

    import ctx._
    ctx.run(infix"""#$schemaSql""".as[Action[Unit]])
    ctx.run(infix"""#$defaultsSql""".as[Action[Unit]])
  }

  override def beforeAll(): Unit = {
    super.beforeAll()
    // Clean test database
    createSchema() match {
      case Failure(exception) =>
        throw new IllegalStateException("Failed to reset database", exception)
      case Success(_) =>
    }
  }

  override def afterAll(): Unit = {
    super.afterAll()
  }
}
