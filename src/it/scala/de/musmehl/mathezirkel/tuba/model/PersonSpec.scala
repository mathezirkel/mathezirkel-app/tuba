package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.DatabaseTestBase

class PersonSpec extends DatabaseTestBase {
  def person = Person.create(
    familyName = "Huber",
    givenName = "Maxi",
    gender = Gender.Male,
    birthDate = Some(java.time.LocalDate.now()),
    addressStreet = Some("Universitätsstr."),
    addressHousenumber = Some("2b"),
    addressPostcode = Some("86165"),
    addressPlace = Some("Augsburg")
  )

  "Person object" should "create, retrieve, update, and destroy a Person object" in {
    val p        = person
    val Some(p1) = Person.retrieve(p.id)
    assert(p === p1)

    val NEW_ADDRESS = "Hannah-Arendt-Straße"

    var affectedRows = Person.update(p.copy(addressStreet = Some(NEW_ADDRESS)))
    val Some(p2)     = Person.retrieve(p.id)
    assert(affectedRows === 1)
    assert(p2.addressStreet === Some(NEW_ADDRESS))

    affectedRows = Person.delete(p2)
    val p3 = Person.retrieve(p.id)
    assert(affectedRows === 1)
    assert(p3 === None)
  }

  "Phone numbers" should "be added, retrieved, and deleted" in {
    val p  = person
    val e1 = Event.createSchoolYear(name = "2019/20")
    val e2 = Event.createSchoolYear(name = "2019/20")

    val num1 = p.addPhoneNumber(
      "0821 123 45 67 89",
      Some("Maja Huber"),
      Some("Festnetz Mutter")
    )
    val num2 = p.addPhoneNumber("0172 000 11 22 33")
    val num3 = p.addPhoneNumber("0821 000 00 00", None, None, Some(e1.id))
    val num4 = p.addPhoneNumber("0161 718 19 20 21 22", None, None, Some(e2.id))

    assert(p.phoneNumbers.toSet === Set(num1, num2))
    assert(p.phoneNumbersForEvent(e1.id).toSet === Set(num1, num2, num3))
    assert(p.phoneNumbersForEvent(e2.id).toSet === Set(num1, num2, num4))

    val affectedRows = PhoneNumber.delete(num1)
    assert(affectedRows === 1)
    assert(p.phoneNumbers === List(num2))
  }

  "Email addresses" should "be added, retrieved, and deleted" in {
    val p = person
    val addr1 = p.addEmailAddress(
      "foo@bar.baz",
      Some("Maja Huber"),
      Some("Nur für Spam benutzen!")
    )
    val addr2           = p.addEmailAddress("bar@baz.foo")
    val mailAddressList = p.emailAddresses
    assert(mailAddressList.toSet === Set(addr1, addr2))

    val affectedRows = EmailAddress.delete(addr1)
    assert(affectedRows === 1)
    assert(p.emailAddresses === List(addr2))
  }

  "Contests for a person" should "be added, retrieved, and deleted" in {
    val c = Contest.create(name = "Toller Preis des Jahres")
    val p = person.withContest(c.id)

    var contests = p.contests

    assert(contests.size === 1)
    assert(contests.head === c.id)

    p.removeContest(c.id)

    contests = p.contests

    assert(contests.size === 0)
  }

  "Tags" should "be added, retrieved, updated, and deleted" in {
    val p = person

    val keyOne = "key"
    val valueOne = "value"
    val keyTwo = "property"
    val valueTwo = "anotherValue"
    p.addTag(keyOne, valueOne)
    p.addTag(keyTwo, valueTwo)

    val tags = p.tags

    assert(tags.keySet.size === 2)
    assert(tags.get(keyOne) === Some(valueOne))
    assert(tags.get(keyTwo) === Some(valueTwo))

    p.removeTag(keyOne)
    p.updateTag(keyTwo, valueOne)

    val tagsAfterEdit = p.tags

    assert(tagsAfterEdit.keySet.size === 1)
    assert(tagsAfterEdit.get(keyTwo) === Some(valueOne))
  }
}
