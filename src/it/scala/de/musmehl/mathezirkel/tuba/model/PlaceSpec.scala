package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.DatabaseTestBase

class PlaceSpec extends DatabaseTestBase {
  "Place object" should "create, retrieve, update, and destroy a Place object" in {
    val p        = Place.create(name = "Uni")
    val Some(p1) = Place.retrieve(p.id)
    assert(p === p1)

    val NEW_NAME = "Violau"

    var affectedRows = Place.update(p.copy(name = NEW_NAME))
    val Some(p2)     = Place.retrieve(p.id)
    assert(affectedRows === 1)
    assert(p2.name === NEW_NAME)

    affectedRows = Place.delete(p2)
    val p3 = Place.retrieve(p.id)
    assert(affectedRows === 1)
    assert(p3 === None)
  }
}
