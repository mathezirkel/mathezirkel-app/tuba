package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.DatabaseTestBase
import org.scalatest.OptionValues._

class IllnessSpec extends DatabaseTestBase {

  "An Illness" should "be retrievable by ID after it was created" in {
    val illness = Illness.create("MyLeastFavoriteDisease")
    assert(Illness.retrieve(illness.id).value === illness)
  }

  it should "be updatable" in {
    val asthma: Illness = Illness.retrieve("Asthma").value

    val anotherIllness = asthma.copy(name = "AnotherIllness")
    Illness.update(anotherIllness)

    assert(Illness.retrieve(asthma.id).value === anotherIllness)
  }

  it should "be deletable" in {
    val schlafwandeln: Illness = Illness.retrieve("Schlafwandeln").value

    Illness.delete(schlafwandeln)

    assert(Illness.retrieve(schlafwandeln.id).isEmpty)
    assert(Illness.retrieve("Schlafwandeln").isEmpty)
  }

}
