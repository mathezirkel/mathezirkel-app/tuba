package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.DatabaseTestBase

class EventSpec extends DatabaseTestBase {
  "Event object" should "create, retrieve, update, and destroy a Event object" in {
    val e        = Event.createSchoolYear(name = "2020/21")
    val Some(e1) = Event.retrieve(e.id)
    assert(e === e1)

    val NEW_NAME = "2019/20"

    var affectedRows = Event.update(e.copy(name = NEW_NAME))
    val Some(e2)     = Event.retrieve(e.id)
    assert(affectedRows === 1)
    assert(e2.name === NEW_NAME)

    affectedRows = Event.delete(e2)
    val e3 = Event.retrieve(e.id)
    assert(affectedRows === 1)
    assert(e3 === None)
  }
}
