package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.DatabaseTestBase

class ParticipantSpec extends DatabaseTestBase {
  "Participants" should "be associated correctly to events" in {
    val p1 = Person.create(familyName = "Maier", givenName = "Anja", gender = Gender.Female)
    val p2 = Person.create(familyName = "Möller", givenName = "Mark", gender = Gender.Male)
    val e  = Event.createSchoolYear(name = "2020/21")
      .withParticipant(p1.id, ParticipantType.Tutor)
      .withParticipant(p2.id, ParticipantType.Student)

    var participants = e.participants

    assert(p1.events === List(e.id))
    assert(p2.events === List(e.id))

    assert(participants.size === 2)

    val Some(participant1) = participants.find(_.person == p1.id)
    assert(participant1.event === e.id)
    assert(participant1.participantType === ParticipantType.Tutor)

    val Some(participant2) = participants.find(_.person == p2.id)
    assert(participant2.event === e.id)
    assert(participant2.participantType === ParticipantType.Student)

    var affectedRows = e.removeParticipant(p2.id)
    assert(affectedRows === 1)

    affectedRows = e.updateParticipantType(p1.id, ParticipantType.Student)
    assert(affectedRows === 1)

    participants = e.participants

    assert(participants.size === 1)
    assert(participants.head.person === p1.id)
    assert(participants.head.participantType === ParticipantType.Student)
  }
}
