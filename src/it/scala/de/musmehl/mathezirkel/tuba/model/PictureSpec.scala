package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.DatabaseTestBase

class PictureSpec extends DatabaseTestBase {
  "Picture object" should "create, retrieve, and destroy a Picture object" in {
    val p        = Picture.create(data = Array[Byte](10, 12, 15, 13));
    val Some(p1) = Picture.retrieve(p.id)
    assert(p.data === p1.data)

    val affectedRows = Picture.delete(p1)
    val p2 = Picture.retrieve(p.id)
    assert(affectedRows === 1)
    assert(p2 === None)
  }
}
