package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.DatabaseTestBase

class RoomSpec extends DatabaseTestBase {
  "Room object" should "create, retrieve, update, and destroy a Room object" in {
    val r = Room.create(
      name = "2006/L1",
      place = Place.create("Uni").id,
      capacity = Some(25)
    )
    val Some(r1) = Room.retrieve(r.id)
    assert(r === r1)

    val NEW_CAPACITY = 20

    var affectedRows = Room.update(r.copy(capacity = Some(NEW_CAPACITY)))
    val Some(r2)     = Room.retrieve(r.id)
    assert(affectedRows === 1)
    assert(r2.capacity === Some(NEW_CAPACITY))

    affectedRows = Room.delete(r2)
    val r3 = Room.retrieve(r.id)
    assert(affectedRows === 1)
    assert(r3 === None)
  }
}
