package de.musmehl.mathezirkel.tuba.model

import java.time.{Duration, OffsetDateTime, ZoneOffset}
import de.musmehl.mathezirkel.tuba.DatabaseTestBase

class AppointmentSpec extends DatabaseTestBase {
  "Appointment object" should "create, retrieve, update, and destroy an Appointment object" in {
    val e = Event.createSchoolYear("2020/21")
    val r = Room.create("1001/T", Place.create("Uni").id)
    val a = Appointment.create(
      e.id,
      Appointment
        .TimeSlot(
          OffsetDateTime
            .now()
            .withOffsetSameInstant(ZoneOffset.UTC)
            .withNano(0),
          OffsetDateTime
            .now()
            .withOffsetSameInstant(ZoneOffset.UTC)
            .withNano(0)
            .plusHours(1)
        ),
      Some(r.id)
    )
    val Some(a1) = Appointment.retrieve(a.id)
    assert(a === a1)

    val NEW_TIME_SLOT = Appointment.TimeSlot(
      OffsetDateTime
        .now()
        .withOffsetSameInstant(ZoneOffset.UTC)
        .withNano(0)
        .minusDays(1),
      OffsetDateTime
        .now()
        .withOffsetSameInstant(ZoneOffset.UTC)
        .withNano(0)
    )

    var affectedRows = Appointment.update(a.copy(timeSlot = NEW_TIME_SLOT))
    val Some(a2)     = Appointment.retrieve(a.id)
    assert(affectedRows === 1)
    assert(a2.timeSlot === NEW_TIME_SLOT)

    affectedRows = Appointment.delete(a2)
    val a3 = Appointment.retrieve(a.id)
    assert(affectedRows === 1)
    assert(a3 === None)
  }

  it should "be able to create several Appointment objects at once" in {
    val e = Event.createSchoolYear("2020/21")
    val r = Room.create("1001/T", Place.create("Uni").id)
    val timeSlot = Appointment.TimeSlot(
      OffsetDateTime
        .now()
        .withOffsetSameInstant(ZoneOffset.UTC)
        .withNano(0),
      OffsetDateTime
        .now()
        .withOffsetSameInstant(ZoneOffset.UTC)
        .withNano(0)
        .plusHours(1)
    )
    val appointments = Appointment.batchCreate(
      event = e.id,
      firstTimeSlot = timeSlot,
      room = Some(r.id),
      interval = Duration.ofDays(1),
      count = 3
    )

    assert(appointments.length === 3)
    assert(appointments.forall(_.event == e.id))
    assert(appointments.forall(_.room contains r.id))
    assert(appointments.exists(_.timeSlot === timeSlot))
    assert(
      appointments.exists(
        _.timeSlot.startDate === timeSlot.startDate.plusDays(1)
      )
    )
    assert(
      appointments.exists(
        _.timeSlot.startDate === timeSlot.startDate.plusDays(2)
      )
    )
  }
}
