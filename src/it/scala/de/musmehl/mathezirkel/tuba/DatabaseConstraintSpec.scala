package de.musmehl.mathezirkel.tuba

import de.musmehl.mathezirkel.tuba.model.{
  Event,
  EventType,
  Gender,
  Place,
  Room,
  SchoolType
}
import io.getquill._
import org.postgresql.util.PSQLException

class DatabaseConstraintSpec extends DatabaseTestBase {

  "Gender" should "be correctly represented in the database" in {
    import ctx._
    import Gender._
    case class DBGender(id: Int, name: String)

    val dbFemaleId = ctx.run(
      querySchema[DBGender]("mathezirkel_db.gender")
        .filter(_.name == lift(Female.name))
        .map(_.id)
    )
    assert(dbFemaleId === List(Female.id))

    val dbMaleId = ctx.run(
      querySchema[DBGender]("mathezirkel_db.gender")
        .filter(_.name == lift(Male.name))
        .map(_.id)
    )
    assert(dbMaleId === List(Male.id))
  }

  "Person table" should "have a foreign key constraint for the gender column" in {
    import ctx._
    val foreignKeyException = intercept[PSQLException] {
      ctx.run(
        infix"""INSERT INTO mathezirkel_db.person (family_name, given_name, gender) VALUES ('', '', 0)"""
          .as[Action[Unit]]
      )
    }
    val INTEGRITY_CONSTRAINT_VIOLATION = "23"
    assert(
      foreignKeyException.getSQLState.startsWith(INTEGRITY_CONSTRAINT_VIOLATION)
    )
  }

  "Room table" should "have a foreign key constraint for the place column" in {
    import ctx._
    val foreignKeyException = intercept[PSQLException] {
      ctx.run(
        infix"""INSERT INTO mathezirkel_db.room (name, place) VALUES ('', 0)"""
          .as[Action[Unit]]
      )
    }
    val INTEGRITY_CONSTRAINT_VIOLATION = "23"
    assert(
      foreignKeyException.getSQLState.startsWith(INTEGRITY_CONSTRAINT_VIOLATION)
    )
  }

  "EventType" should "be correctly represented in the database" in {
    import ctx._
    import EventType._
    case class DBEventType(id: Int, name: String)

    val dbSchoolYearId = ctx.run(
      querySchema[DBEventType]("mathezirkel_db.event_type")
        .filter(_.name == lift(SchoolYear.name))
        .map(_.id)
    )
    assert(dbSchoolYearId === List(SchoolYear.id))

    val dbCampId = ctx.run(
      querySchema[DBEventType]("mathezirkel_db.event_type")
        .filter(_.name == lift(Camp.name))
        .map(_.id)
    )
    assert(dbCampId === List(Camp.id))

    val dbCircleId = ctx.run(
      querySchema[DBEventType]("mathezirkel_db.event_type")
        .filter(_.name == lift(Circle.name))
        .map(_.id)
    )
    assert(dbCircleId === List(Circle.id))

    val dbActivityId = ctx.run(
      querySchema[DBEventType]("mathezirkel_db.event_type")
        .filter(_.name == lift(Activity.name))
        .map(_.id)
    )
    assert(dbActivityId === List(Activity.id))

    val dbOtherId = ctx.run(
      querySchema[DBEventType]("mathezirkel_db.event_type")
        .filter(_.name == lift(Other.name))
        .map(_.id)
    )
    assert(dbOtherId === List(Other.id))
  }

  "School type" should "be correctly represented in the database" in {
    import ctx._
    import SchoolType._
    case class DBSchoolType(id: Int, name: String)

    val dbGymnasiumId = ctx.run(
      querySchema[DBSchoolType]("mathezirkel_db.school_type")
        .filter(_.name == lift(Gymnasium.name))
        .map(_.id)
    )
    assert(dbGymnasiumId === List(Gymnasium.id))

    val dbRealschuleId = ctx.run(
      querySchema[DBSchoolType]("mathezirkel_db.school_type")
        .filter(_.name == lift(Realschule.name))
        .map(_.id)
    )
    assert(dbRealschuleId === List(Realschule.id))

    val dbMittelschuleId = ctx.run(
      querySchema[DBSchoolType]("mathezirkel_db.school_type")
        .filter(_.name == lift(Mittelschule.name))
        .map(_.id)
    )
    assert(dbMittelschuleId === List(Mittelschule.id))
  }

  "School table" should "have a foreign key constraint for the school_type column" in {
    import ctx._
    val foreignKeyException = intercept[PSQLException] {
      ctx.run(
        infix"""INSERT INTO mathezirkel_db.school (name, school_type) VALUES ('', 0)"""
          .as[Action[Unit]]
      )
    }
    val INTEGRITY_CONSTRAINT_VIOLATION = "23"
    assert(
      foreignKeyException.getSQLState.startsWith(INTEGRITY_CONSTRAINT_VIOLATION)
    )
  }

  "Event table" should "have a foreign key constraint for the parent column" in {
    import ctx._
    val q = Event.retrieve(Event.ID(0))

    q match {
      case Some(e) =>
        Event.delete(
          e
        ) // Make sure that no Event with id 0 exists in the database
      case None =>
    }

    val foreignKeyException = intercept[PSQLException] {
      ctx.run(
        infix"""INSERT INTO mathezirkel_db.event (name, event_type, parent) VALUES ('', 1, 0)"""
          .as[Action[Unit]]
      )
    }
    val INTEGRITY_CONSTRAINT_VIOLATION = "23"
    assert(
      foreignKeyException.getSQLState.startsWith(INTEGRITY_CONSTRAINT_VIOLATION)
    )
  }

  "Appointment table" should "have a foreign key constraint for the event column" in {
    import ctx._
    val roomID = Room.create("2006/L1", Place.create("Uni").id).id.id
    val foreignKeyException = intercept[PSQLException] {
      ctx.run(
        infix"""INSERT INTO mathezirkel_db.appointment (event, start_date, end_date, room) VALUES (0, 0, 0, #$roomID)"""
          .as[Action[Unit]]
      )
    }
    val INTEGRITY_CONSTRAINT_VIOLATION = "23"
    assert(
      foreignKeyException.getSQLState.startsWith(INTEGRITY_CONSTRAINT_VIOLATION)
    )
  }

  it should "have a foreign key constraint for the room column" in {
    import ctx._
    val eventID = Event.createSchoolYear("2020/21").id.id
    val foreignKeyException = intercept[PSQLException] {
      ctx.run(
        infix"""INSERT INTO mathezirkel_db.appointment (event, start_date, end_date, room) VALUES (#$eventID, 0, 0, 0)"""
          .as[Action[Unit]]
      )
    }
    val INTEGRITY_CONSTRAINT_VIOLATION = "23"
    assert(
      foreignKeyException.getSQLState.startsWith(INTEGRITY_CONSTRAINT_VIOLATION)
    )
  }
}
