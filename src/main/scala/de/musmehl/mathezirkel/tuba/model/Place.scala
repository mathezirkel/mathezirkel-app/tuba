package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.PostgresContext

case class Place(id: Place.ID, name: String)

object Place {

  /** Unique identifier of a Place */
  case class ID(id: Int) extends AnyVal

  /** Return the query which contains all places */
  def all(implicit ctx: PostgresContext) = {
    import ctx._
    quote { querySchema[Place]("mathezirkel_db.place") }
  }

  /** Retrieve a place given its id */
  def retrieve(id: Place.ID)(implicit ctx: PostgresContext): Option[Place] = {
    import ctx._
    ctx.run(all.filter(_.id == lift(id))).headOption
  }

  /** Create a place
    *
    *  @return the created Place
    */
  def create(name: String)(implicit ctx: PostgresContext): Place = {
    import ctx._
    val place = Place(Place.ID(0), name)
    place.copy(id = ctx.run(all.insert(lift(place)).returningGenerated(_.id)))
  }

  /** Update place's data in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def update(place: Place)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(all.filter(_.id == lift(place.id)).update(lift(place)))
  }

  /** Delete a place in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def delete(place: Place)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(all.filter(_.id == lift(place.id)).delete)
  }

}
