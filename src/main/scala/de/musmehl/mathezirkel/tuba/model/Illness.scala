/* Copyright (C) 2020 Sven Prüfer - All Rights Reserved
 *
 * This file is part of Tuba.
 *
 * Tuba is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tuba is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tuba.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.PostgresContext

/** An illness that a person can have. */
case class Illness(id: Illness.ID, name: String)

object Illness {

  /** Typed ID of an [[Illness]]. */
  final case class ID(value: Int) extends AnyVal

  /** Query yielding all [[Illness Illnesses]] */
  def all(implicit ctx: PostgresContext) = {
    import ctx._
    quote { querySchema[Illness]("mathezirkel_db.illness") }
  }

  /** Retrieve an [[Illness]] given its id */
  def retrieve(
      id: Illness.ID
  )(implicit ctx: PostgresContext): Option[Illness] = {
    import ctx._
    ctx.run(all.filter(_.id == lift(id))).headOption
  }

  /** Retrieve an [[Illness]] given its name */
  def retrieve(name: String)(implicit ctx: PostgresContext): Option[Illness] = {
    import ctx._
    ctx.run(all.filter(_.name == lift(name))).headOption
  }

  /** Create an [[Illness]]
    *
    * @return the created illness
    */
  def create(
      name: String
  )(implicit ctx: PostgresContext): Illness = {
    import ctx._
    val illness = Illness(Illness.ID(0), name)
    Illness(
      ctx.run(
        all.insert(lift(illness)).returningGenerated(_.id)
      ),
      name
    )
  }

  /** Delete an [[Illness]] in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def delete(illness: Illness)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(all.filter(_.id == lift(illness.id)).delete)
  }

  /** Update an [[Illness]] in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def update(illness: Illness)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(all.filter(_.id == lift(illness.id)).update(lift(illness)))
  }
}
