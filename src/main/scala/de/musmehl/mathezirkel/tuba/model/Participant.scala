package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.PostgresContext

/** A participant associates a [[Person]] to an [[Event]]
  */
case class Participant(
    event: Event.ID,
    person: Person.ID,
    participantType: ParticipantType
)

object Participant {

  /** Return the query which contains all participants */
  def all(implicit ctx: PostgresContext) = {
    import ctx._
    quote { querySchema[Participant]("mathezirkel_db.participant") }
  }

  /** Create a participant
    *
    *  @return the created [[Participant]]
    */
  def create(
      event: Event.ID,
      person: Person.ID,
      participantType: ParticipantType
  )(implicit ctx: PostgresContext): Participant = {
    import ctx._
    val participant = Participant(event, person, participantType)
    ctx.run(all.insert(lift(participant)))
    participant
  }

  /** Update a participant in the database. Note that it is only possible to update the [[ParticipantType]] because the
    * participant is identified uniquely by the [[Event]] and the [[Person]]. For other updates, the [[Participant]] has
    * to be deleted and created again.
    *
    *  @return the number of affected rows (1 if successful)
    */
  def update(participant: Participant)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx
      .run(
        all
          .filter(p =>
            p.event == lift(participant.event) && p.person == lift(
              participant.person
            )
          )
          .update(lift(participant))
      )
  }

  /** Delete a participant from the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def delete(event: Event.ID, person: Person.ID)(implicit
      ctx: PostgresContext
  ): Long = {
    import ctx._
    ctx
      .run(
        all
          .filter(p =>
            p.event == lift(event) && p.person == lift(
              person
            )
          )
          .delete
      )
  }

}
