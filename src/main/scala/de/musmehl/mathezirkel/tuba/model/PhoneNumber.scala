package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.PostgresContext

/** A Phone number associated to a Person */
case class PhoneNumber(
    personId: Person.ID,
    phoneNumber: String,
    owner: Option[String],
    comment: Option[String],
    eventId: Option[Event.ID]
)

object PhoneNumber {

  /** Return the query which contains all phone numbers */
  def all(implicit ctx: PostgresContext) = {
    import ctx._
    quote { querySchema[PhoneNumber]("mathezirkel_db.phone_number") }
  }

  /** Retrieve the list of phone numbers for a given person, which are not associated to an event */
  def forPersonID(
      personId: Person.ID
  )(implicit ctx: PostgresContext): List[PhoneNumber] =
    forPersonAndEvent(personId, None)

  /** Retrieve the list of phone numbers for a given person and an specific event */
  def forPersonAndEvent(
      personId: Person.ID,
      eventId: Option[Event.ID]
  )(implicit ctx: PostgresContext): List[PhoneNumber] = {
    import ctx._
    ctx.run(
      all
        .filter(num =>
          num.personId == lift(
            personId
          ) && num.eventId == lift(
            eventId
          )
        )
    )
  }

  /** Create a phone number for a Person. */
  def create(
      personId: Person.ID,
      phoneNumber: String,
      owner: Option[String] = None,
      comment: Option[String] = None,
      eventId: Option[Event.ID] = None
  )(implicit ctx: PostgresContext): PhoneNumber = {
    import ctx._
    val number = PhoneNumber(personId, phoneNumber, owner, comment, eventId)
    ctx.run(all.insert(lift(number)))
    number
  }

  /** Delete a phone number in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def delete(phoneNumber: PhoneNumber)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(
      all
        .filter(num =>
          num.personId == lift(
            phoneNumber.personId
          ) && num.phoneNumber == lift(
            phoneNumber.phoneNumber
          ) && num.eventId == lift(
            phoneNumber.eventId
          )
        )
        .delete
    )
  }
}
