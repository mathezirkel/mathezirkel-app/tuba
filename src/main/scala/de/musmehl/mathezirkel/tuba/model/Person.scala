package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.PostgresContext

/** Everyone is a Person :)
  *
  *  The entries are obvious, apart from "otherTags", which is stored in JSON format in the database, and which can represent all
  *  kinds of interesting information about a Person.
  */
case class Person(
    id: Person.ID,
    familyName: String,
    givenName: String,
    gender: Gender,
    birthDate: Option[java.time.LocalDate],
    addressStreet: Option[String],
    addressHousenumber: Option[String],
    addressPostcode: Option[String],
    addressPlace: Option[String]
) {

  /** Obtain the list of all phone numbers for the person */
  def phoneNumbers(implicit ctx: PostgresContext): List[PhoneNumber] =
    PhoneNumber.forPersonID(id)

  /** Obtain the list of all phone numbers for the person which are either associated to the given event or to no event at all */
  def phoneNumbersForEvent(
      eventId: Event.ID
  )(implicit ctx: PostgresContext): List[PhoneNumber] =
    PhoneNumber.forPersonAndEvent(id, Some(eventId)).appendedAll(phoneNumbers)

  /** Add a new phone number for this person. */
  def addPhoneNumber(
      phoneNumber: String,
      owner: Option[String] = None,
      comment: Option[String] = None,
      event: Option[Event.ID] = None
  )(implicit ctx: PostgresContext): PhoneNumber =
    PhoneNumber.create(id, phoneNumber, owner, comment, event)

  /** Obtain the list of all email addresses for the person */
  def emailAddresses(implicit ctx: PostgresContext): List[EmailAddress] =
    EmailAddress.forPersonID(id)

  /** Add a new email address for this person. */
  def addEmailAddress(
      emailAddress: String,
      owner: Option[String] = None,
      comment: Option[String] = None
  )(implicit ctx: PostgresContext): EmailAddress =
    EmailAddress.create(id, emailAddress, owner, comment)

  /** Associate a [[Contest]] to the [[Person]]
    *
    * @return the [[Person]] itself, allowing for chaining
    */
  def withContest(
      contest: Contest.ID
  )(implicit ctx: PostgresContext): Person = {
    ContestParticipant.create(contest, id)
    this
  }

  /** Remove the [[Contest]] participation from the [[Person]]
    *
    * @return the [[Person]] itself, allowing for chaining
    */
  def removeContest(
      contest: Contest.ID
  )(implicit ctx: PostgresContext): Person = {
    ContestParticipant.delete(
      ContestParticipant(contest = contest, person = id)
    )
    this
  }

  /** @return the list of IDs of [[Contest Contests]] in which this [[Person]] participates
    */
  def contests(implicit ctx: PostgresContext): List[Contest.ID] = {
    ContestParticipant.forPersonID(id).map(_.contest)
  }

  def events(implicit ctx: PostgresContext): List[Event.ID] = {
    import ctx._
    ctx.run(Participant.all.filter(_.person == lift(id)).map(_.event))
  }

  /** List all tags for this person */
  def tags(implicit ctx: PostgresContext): Map[String, String] =
    PersonTag.forPersonID(id)

  /** Add a tag for this person */
  def addTag(key: String, value: String)(implicit
      ctx: PostgresContext
  ): Unit = {
    PersonTag.create(id, key, value)
  }

  /** Update a tag for this person */
  def updateTag(key: String, value: String)(implicit
      ctx: PostgresContext
  ): Unit = {
    PersonTag.update(id, key, value)
  }

  /** Remove a tag for this person */
  def removeTag(key: String)(implicit ctx: PostgresContext): Unit = {
    PersonTag.delete(id, key)
  }

}

object Person {

  /** Unique identifier of a Person */
  case class ID(id: Int) extends AnyVal

  /** Return the query which contains all persons */
  def all(implicit ctx: PostgresContext) = {
    import ctx._
    quote { querySchema[Person]("mathezirkel_db.person") }
  }

  /** Retrieve a person given their id */
  def retrieve(id: Person.ID)(implicit ctx: PostgresContext): Option[Person] = {
    import ctx._
    ctx.run(all.filter(_.id == lift(id))).headOption
  }

  /** Create a person
    *
    *  @return the created Person
    */
  def create(
      familyName: String,
      givenName: String,
      gender: Gender,
      birthDate: Option[java.time.LocalDate] = None,
      addressStreet: Option[String] = None,
      addressHousenumber: Option[String] = None,
      addressPostcode: Option[String] = None,
      addressPlace: Option[String] = None
  )(implicit ctx: PostgresContext): Person = {
    import ctx._
    val person = Person(
      Person.ID(0),
      familyName,
      givenName,
      gender,
      birthDate,
      addressStreet,
      addressHousenumber,
      addressPostcode,
      addressPlace
    )
    person.copy(id = ctx.run(all.insert(lift(person)).returningGenerated(_.id)))
  }

  /** Update person's data in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def update(person: Person)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(all.filter(_.id == lift(person.id)).update(lift(person)))
  }

  /** Delete a person in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def delete(person: Person)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(all.filter(_.id == lift(person.id)).delete)
  }

}
