package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.PostgresContext

/** This class represents a school of type schoolType */
case class School(
    id: School.ID,
    name: String,
    schoolType: SchoolType,
    addressStreet: Option[String],
    addressHousenumber: Option[String],
    addressPostcode: Option[String],
    addressPlace: Option[String],
    officialId: Option[String],
    numberMathTeachers: Option[Int]
)

object School {
  import SchoolType._

  /** Unique identifier of a School */
  case class ID(id: Int) extends AnyVal

  /** Return the query which contains all schools */
  def all(implicit ctx: PostgresContext) = {
    import ctx._
    quote { querySchema[School]("mathezirkel_db.school") }
  }

  /** Retrieve a school given its id */
  def retrieve(id: School.ID)(implicit ctx: PostgresContext): Option[School] = {
    import ctx._
    ctx.run(all.filter(_.id == lift(id))).headOption
  }

  /** Create a school
    *
    *  @return the created School
    */
  def create(
      name: String,
      schoolType: SchoolType,
      addressStreet: Option[String] = None,
      addressHousenumber: Option[String] = None,
      addressPostcode: Option[String] = None,
      addressPlace: Option[String] = None,
      officialId: Option[String] = None,
      numberMathTeachers: Option[Int] = None
  )(implicit ctx: PostgresContext): School = {
    import ctx._
    val school = School(
      School.ID(0),
      name,
      schoolType,
      addressStreet,
      addressHousenumber,
      addressPostcode,
      addressPlace,
      officialId,
      numberMathTeachers
    )
    school.copy(id = ctx.run(all.insert(lift(school)).returningGenerated(_.id)))
  }

  /** Update school's data in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def update(school: School)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(all.filter(_.id == lift(school.id)).update(lift(school)))
  }

  /** Delete a school in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def delete(school: School)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(all.filter(_.id == lift(school.id)).delete)
  }

}
