package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.PostgresContext

/** A Room is something where events of all kinds take place. It is associated to a Place (Uni or Violau). */
case class Room(
    id: Room.ID,
    name: String,
    place: Place.ID,
    capacity: Option[Int]
)

object Room {

  /** Unique identifier of a Room */
  case class ID(id: Int) extends AnyVal

  /** Return the query which contains all rooms */
  def all(implicit ctx: PostgresContext) = {
    import ctx._
    quote { querySchema[Room]("mathezirkel_db.room") }
  }

  /** Retrieve a room given its id */
  def retrieve(id: Room.ID)(implicit ctx: PostgresContext): Option[Room] = {
    import ctx._
    ctx.run(all.filter(_.id == lift(id))).headOption
  }

  /** Create a room
    *
    *  @return the created Room
    */
  def create(
      name: String,
      place: Place.ID,
      capacity: Option[Int] = None
  )(implicit ctx: PostgresContext): Room = {
    import ctx._
    val room = Room(Room.ID(0), name, place, capacity)
    room.copy(id = ctx.run(all.insert(lift(room)).returningGenerated(_.id)))
  }

  /** Update room's data in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def update(room: Room)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(all.filter(_.id == lift(room.id)).update(lift(room)))
  }

  /** Delete a room in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def delete(room: Room)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(all.filter(_.id == lift(room.id)).delete)
  }

}
