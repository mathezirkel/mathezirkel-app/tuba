package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.PostgresContext

case class Contest(id: Contest.ID, name: String)

object Contest {

  /** Unique identifier of a [[Contest]] */
  case class ID(id: Int) extends AnyVal

  /** Return the query which contains all [[Contest Contests]] */
  def all(implicit ctx: PostgresContext) = {
    import ctx._
    quote { querySchema[Contest]("mathezirkel_db.contest") }
  }

  /** Retrieve a [[Contest]] given its id */
  def retrieve(
      id: Contest.ID
  )(implicit ctx: PostgresContext): Option[Contest] = {
    import ctx._
    ctx.run(all.filter(_.id == lift(id))).headOption
  }

  /** Create a [[Contest]]
    *
    *  @return the created [[Contest]]
    */
  def create(name: String)(implicit ctx: PostgresContext): Contest = {
    import ctx._
    val contest = Contest(Contest.ID(0), name)
    contest.copy(id =
      ctx.run(all.insert(lift(contest)).returningGenerated(_.id))
    )
  }

  /** Update [[Contest Contest's]] data in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def update(contest: Contest)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(all.filter(_.id == lift(contest.id)).update(lift(contest)))
  }

  /** Delete a [[Contest]] in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def delete(contest: Contest)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(all.filter(_.id == lift(contest.id)).delete)
  }

}
