package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.PostgresContext

/** This class wraps a picture, represented as byte array
  */
case class Picture(
    id: Picture.ID,
    data: Array[Byte]
)

object Picture {

  /** Unique identifier of a [[Picture]]
    */
  case class ID(id: Int) extends AnyVal;

  /** Return the query which contains all pictures */
  def all(implicit ctx: PostgresContext) = {
    import ctx._
    quote { querySchema[Picture]("mathezirkel_db.picture") }
  }

  /** Retrieve a picture given its id */
  def retrieve(
      id: Picture.ID
  )(implicit ctx: PostgresContext): Option[Picture] = {
    import ctx._
    ctx.run(all.filter(_.id == lift(id))).headOption
  }

  /** Create a picture
    *
    *  @return the created Picture
    */
  def create(data: Array[Byte])(implicit ctx: PostgresContext): Picture = {
    import ctx._
    val picture = Picture(
      Picture.ID(0),
      data
    )
    picture.copy(
      id = ctx.run(all.insert(lift(picture)).returningGenerated(_.id))
    )
  }

  /** Delete a picture in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def delete(picture: Picture)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(all.filter(_.id == lift(picture.id)).delete)
  }
}
