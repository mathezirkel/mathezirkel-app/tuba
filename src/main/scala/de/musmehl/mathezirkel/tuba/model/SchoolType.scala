package de.musmehl.mathezirkel.tuba.model

import io.getquill._

sealed trait SchoolType { val id: Int; val name: String }

object SchoolType {
  case object Gymnasium extends SchoolType {
    val id = 1; val name = "Gymnasium"
  }
  case object Realschule extends SchoolType {
    val id = 2; val name = "Realschule"
  }
  case object Mittelschule extends SchoolType {
    val id = 3; val name = "Mittelschule"
  }

  implicit val encodeSchoolType = MappedEncoding[SchoolType, Int](_.id)
  implicit val decodeSchoolType = MappedEncoding[Int, SchoolType] {
    case Gymnasium.id    => Gymnasium
    case Realschule.id   => Realschule
    case Mittelschule.id => Mittelschule
  }
}
