package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.PostgresContext

/** An [[Event]] is an arbitrary group of meetings.
  *
  *  There can be various types of events which are documented in the EventType class.
  *  Apart from a [[SchoolYear]], every [[Event]] has a parent [[Event]] in which it is contained.
  *  An event can have (possibly multiple) associated [[Appointment]]s which associate a room and a time slot to an event.
  */
sealed trait Event {
  def id: Event.ID
  def name: String
  def parent: Option[Event.ID]
  def eventType: EventType
  def toGenericEvent: GenericEvent = GenericEvent(id, name, None, eventType)
  def participants(implicit ctx: PostgresContext): List[Participant] = {
    import ctx._
    ctx.run(Participant.all.filter(_.event == lift(id)))
  }
  def withParticipant(person: Person.ID, participantType: ParticipantType)(
      implicit ctx: PostgresContext
  ): Event = {
    Participant.create(id, person, participantType)
    this
  }
  def removeParticipant(person: Person.ID)(implicit
      ctx: PostgresContext
  ): Long = Participant.delete(id, person)
  def updateParticipantType(
      person: Person.ID,
      participantType: ParticipantType
  )(implicit ctx: PostgresContext): Long =
    Participant.update(Participant(id, person, participantType))
}

/** A school year, e.g. "2019/20" */
case class SchoolYear(id: Event.ID, name: String) extends Event {
  override def parent    = None
  override def eventType = EventType.SchoolYear
}

/** A math camp, e.g. "Mathecamp Violau 2020"
  *
  *  A math camp should have a SchoolYear as parent event
  */
case class Camp(id: Event.ID, name: String, parentID: Event.ID) extends Event {
  override def parent    = Some(parentID)
  override def eventType = EventType.Camp
}

/** A math circle, e.g. "Präsenzzirkel 7/8 c"
  *
  *  A circle should have a SchoolYear or a Camp as parent event
  */
case class Circle(id: Event.ID, name: String, parentID: Event.ID)
    extends Event {
  override def parent    = Some(parentID)
  override def eventType = EventType.Circle
}

/** A non-math activity, e.g. "Karaoke" */
case class Activity(id: Event.ID, name: String, parentID: Event.ID)
    extends Event {
  override def parent    = Some(parentID)
  override def eventType = EventType.Activity
}

/** Any kind of event which does not fit any of the above, e.g. "Mai-Mathetag 2020" */
case class Other(id: Event.ID, name: String, parentID: Event.ID) extends Event {
  override def parent    = Some(parentID)
  override def eventType = EventType.Other
}

/** A generic event, used for querying */
case class GenericEvent(
    id: Event.ID,
    name: String,
    parent: Option[Event.ID],
    eventType: EventType
) {
  def toConcreteEvent: Event = {
    eventType match {
      case EventType.SchoolYear => SchoolYear(id, name)
      case EventType.Camp       => Camp(id, name, parent.get)
      case EventType.Circle     => Circle(id, name, parent.get)
      case EventType.Activity   => Activity(id, name, parent.get)
      case EventType.Other      => Other(id, name, parent.get)
    }
  }
}

object Event {

  /** Unique identifier of a Event */
  case class ID(id: Int) extends AnyVal

  /** Return the query which contains all events */
  def all(implicit ctx: PostgresContext) = {
    import ctx._
    quote {
      querySchema[GenericEvent]("mathezirkel_db.event")
    }
  }

  /** Retrieve an event given its id */
  def retrieve(id: Event.ID)(implicit ctx: PostgresContext): Option[Event] = {
    import ctx._
    ctx.run(all.filter(_.id == lift(id))).headOption.map(_.toConcreteEvent)
  }

  /** Create a generic event
    *
    * @return the id of the created Event
    */
  private def createGeneric(
      name: String,
      eventType: EventType,
      parent: Option[
        Event.ID
      ] // No default value here because the standard should be that an event possesses a parent event.
  )(implicit ctx: PostgresContext): Event.ID = {

    import ctx._
    import EventType._

    ctx.run(
      all
        .insert(lift(GenericEvent(Event.ID(0), name, parent, eventType)))
        .returningGenerated(_.id)
    )
  }

  /** Create a school year
    *
    * @return the created school year
    */
  def createSchoolYear(name: String)(implicit
      ctx: PostgresContext
  ): SchoolYear =
    SchoolYear(createGeneric(name, EventType.SchoolYear, None), name)

  /** Create an event of type Other
    *
    * @return the created event
    */
  def createOther(name: String, parent: Event.ID)(implicit
      ctx: PostgresContext
  ): Other =
    Other(createGeneric(name, EventType.Other, Some(parent)), name, parent)

  /** Update event's data in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def update(event: Event)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(
      all.filter(_.id == lift(event.id)).update(lift(event.toGenericEvent))
    )
  }

  /** Delete an event in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def delete(event: Event)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(all.filter(_.id == lift(event.id)).delete)
  }

}
