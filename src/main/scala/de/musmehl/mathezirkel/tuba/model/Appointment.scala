package de.musmehl.mathezirkel.tuba.model

import java.time.{Duration, Instant, OffsetDateTime, ZoneId}

import de.musmehl.mathezirkel.tuba.PostgresContext
import io.getquill.{Embedded, MappedEncoding}

/** An [[Appointment]] associates to an [[Event]] a date-time and a [[Room]]
  */
case class Appointment(
    id: Appointment.ID,
    event: Event.ID,
    timeSlot: Appointment.TimeSlot,
    room: Option[Room.ID]
)

object Appointment {

  /** Unique identifier of an [[Appointment]] */
  case class ID(id: Int) extends AnyVal

  /** A [[TimeSlot]] has a start and an end [[java.time.OffsetDateTime]] */
  case class TimeSlot(startDate: OffsetDateTime, endDate: OffsetDateTime)
      extends Embedded {
    def +(that: Duration): TimeSlot =
      TimeSlot(this.startDate.plus(that), this.endDate.plus(that))
  }

  // Encoders and decoders for [[ZonedDateTime]]
  implicit val encodeDict = MappedEncoding[OffsetDateTime, Long](
    _.toInstant.toEpochMilli
  )
  implicit val decodeDict = MappedEncoding[Long, OffsetDateTime](timestamp =>
    OffsetDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneId.of("UTC"))
  )

  /** The query which contains all [[TimeSlot]]s */
  def all(implicit ctx: PostgresContext) = {
    import ctx._
    quote { querySchema[Appointment]("mathezirkel_db.appointment") }
  }

  /** Retrieve an [[Appointment]] given its id */
  def retrieve(
      id: Appointment.ID
  )(implicit ctx: PostgresContext): Option[Appointment] = {
    import ctx._
    ctx.run(all.filter(_.id == lift(id))).headOption
  }

  /** Create an [[Appointment]]
    *
    *  @return the created [[Appointment]]
    */
  def create(
      event: Event.ID,
      timeSlot: TimeSlot,
      room: Option[Room.ID]
  )(implicit ctx: PostgresContext): Appointment = {
    import ctx._
    val appointment = Appointment(Appointment.ID(0), event, timeSlot, room)
    appointment.copy(id =
      ctx.run(all.insert(lift(appointment)).returningGenerated(_.id))
    )
  }

  /** Create several periodic [[Appointment Appointments]] for the same event
    * @param event the ID of the associated event,
    * @param firstTimeSlot the time slot for the first appointment,
    * @param room the room ID for the appointments,
    * @param interval the interval between two appointments,
    * @param count the number of appointments to be created
    * @return the [[scala.List]] of [[Appointment Appointments]] which were created.
    */
  def batchCreate(
      event: Event.ID,
      firstTimeSlot: TimeSlot,
      room: Option[Room.ID],
      interval: Duration,
      count: Int
  )(implicit ctx: PostgresContext): List[Appointment] = {
    (1 to count)
      .map(index =>
        create(event, firstTimeSlot + interval.multipliedBy(index - 1), room)
      )
      .toList
  }

  /** Update an [[Appointment]]'s data in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def update(appointment: Appointment)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(all.filter(_.id == lift(appointment.id)).update(lift(appointment)))
  }

  /** Delete an [[Appointment]] in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def delete(appointment: Appointment)(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(all.filter(_.id == lift(appointment.id)).delete)
  }

}
