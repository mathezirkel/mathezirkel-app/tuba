package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.PostgresContext

/** An email address associated to a Person */
case class EmailAddress(
    personId: Person.ID,
    emailAddress: String,
    owner: Option[String],
    comment: Option[String]
)

object EmailAddress {

  /** Return the query which contains all email addresses */
  def all(implicit ctx: PostgresContext) = {
    import ctx._
    quote { querySchema[EmailAddress]("mathezirkel_db.email_address") }
  }

  /** Retrieve the list of email addresses for a given person */
  def forPersonID(
      id: Person.ID
  )(implicit ctx: PostgresContext): List[EmailAddress] = {
    import ctx._
    ctx.run(all.filter(_.personId == lift(id)))
  }

  /** Create an email address for a Person. */
  def create(
      personId: Person.ID,
      emailAddress: String,
      owner: Option[String] = None,
      comment: Option[String] = None
  )(implicit ctx: PostgresContext): EmailAddress = {
    import ctx._
    val address = EmailAddress(personId, emailAddress, owner, comment)
    ctx.run(all.insert(lift(address)))
    address
  }

  /** Delete an email address in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def delete(
      emailAddress: EmailAddress
  )(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(
      all
        .filter(addr =>
          addr.personId == lift(
            emailAddress.personId
          ) && addr.emailAddress == lift(
            emailAddress.emailAddress
          )
        )
        .delete
    )
  }
}
