package de.musmehl.mathezirkel.tuba.model

import io.getquill._

sealed trait EventType { val id: Int; val name: String }

object EventType {

  /** A school year, e.g. "2019/20" */
  case object SchoolYear extends EventType {
    val id = 1; val name = "school year"
  }

  /** A math camp, e.g. "Mathecamp Violau 2020"
    *
    *  A math camp should have a SchoolYear as parent event
    */
  case object Camp extends EventType { val id = 2; val name = "math camp" }

  /** A math circle, e.g. "Präsenzzirkel 7/8 c"
    *
    *  A circle should have a SchoolYear or a Camp as parent event
    */
  case object Circle extends EventType { val id = 3; val name = "math circle" }

  /** A non-math activity, e.g. "Karaoke" */
  case object Activity extends EventType { val id = 4; val name = "activity" }

  /** Any kind of event which does not fit any of the above, e.g. "Mai-Mathetag 2020" */
  case object Other extends EventType { val id = 5; val name = "other" }

  implicit val encodeEventType = MappedEncoding[EventType, Int](_.id)
  implicit val decodeEventType = MappedEncoding[Int, EventType] {
    case SchoolYear.id => SchoolYear
    case Camp.id       => Camp
    case Circle.id     => Circle
    case Activity.id   => Activity
    case Other.id      => Other
  }
}
