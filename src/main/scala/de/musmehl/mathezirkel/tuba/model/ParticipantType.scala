package de.musmehl.mathezirkel.tuba.model

import io.getquill.MappedEncoding

sealed trait ParticipantType { val id: Int; val name: String }

object ParticipantType {
  case object Tutor extends ParticipantType {
    val id = 1; val name = "Tutor"
  }
  case object Student extends ParticipantType {
    val id = 2; val name = "Student"
  }

  implicit val encodeParticipantType =
    MappedEncoding[ParticipantType, Int](_.id)
  implicit val decodeParticipantType = MappedEncoding[Int, ParticipantType] {
    case Tutor.id   => Tutor
    case Student.id => Student
  }
}
