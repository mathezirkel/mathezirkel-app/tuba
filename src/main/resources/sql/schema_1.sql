/* Copyright (C) 2019 Sven Prüfer - All Rights Reserved
 *
 * This file is part of Tuba.
 *
 * Tuba is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tuba is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tuba.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Basic SQL Database Schema for Tuba.
 *
 * This is the initial database schema for Tuba. Execute it on a database to either reset that database or initialize it.
 */

-- Schema
DROP SCHEMA IF EXISTS mathezirkel_db CASCADE;
CREATE SCHEMA mathezirkel_db;
SET SCHEMA 'mathezirkel_db';

-- Gender
CREATE TABLE gender(
    id SERIAL PRIMARY KEY,
    name VARCHAR(32) NOT NULL
);
INSERT INTO gender VALUES (1, 'Female');
INSERT INTO gender VALUES (2, 'Male');

-- Person
CREATE TABLE person(
    id SERIAL PRIMARY KEY,
    family_name VARCHAR(128) NOT NULL,
    given_name VARCHAR(128) NOT NULL,
    gender INT NOT NULL,
    birth_date DATE,
    address_street VARCHAR(128),
    address_housenumber VARCHAR(32),
    address_postcode VARCHAR(32),
    address_place VARCHAR(128),
    FOREIGN KEY (gender) REFERENCES gender(id)
);

CREATE TABLE email_address(
    person_id INT NOT NULL,
    email_address VARCHAR(256) NOT NULL,
    owner VARCHAR(128),
    comment VARCHAR(512),
    CONSTRAINT pk_email_address PRIMARY KEY (person_id, email_address),
    FOREIGN KEY (person_id) REFERENCES person(id)
);

CREATE TABLE person_tag(
    person_id INT NOT NULL,
    key VARCHAR(128) NOT NULL,
    value VARCHAR(512) NOT NULL,
    CONSTRAINT pk_person_tag PRIMARY KEY (person_id, key),
    FOREIGN KEY (person_id) REFERENCES person(id)
);

-- Place
CREATE TABLE place(
    id SERIAL PRIMARY KEY,
    name VARCHAR(32) NOT NULL
);

-- Room
CREATE TABLE room(
    id SERIAL PRIMARY KEY,
    name VARCHAR(128) NOT NULL,
    place INT NOT NULL,
    capacity INT,
    FOREIGN KEY (place) REFERENCES place(id)
);

-- EventType
CREATE TABLE event_type(
    id SERIAL PRIMARY KEY,
    name VARCHAR(32) NOT NULL
);
INSERT INTO event_type VALUES (1, 'school year');
INSERT INTO event_type VALUES (2, 'math camp');
INSERT INTO event_type VALUES (3, 'math circle');
INSERT INTO event_type VALUES (4, 'activity');
INSERT INTO event_type VALUES (5, 'other');

-- Event
-- Check if an event has a school year as ancestor
CREATE FUNCTION check_event(event_type_id INT, parent_id INT, new_id INT)
RETURNS BOOLEAN
LANGUAGE plpgsql
AS $$
DECLARE
    event_type_name VARCHAR(128);
    parent_event_type_id INT;
    grandparent_id INT;
BEGIN
    SET SCHEMA 'mathezirkel_db';
    SELECT event_type.name INTO event_type_name FROM event_type WHERE id = event_type_id;
    IF event_type_name = 'school year' THEN
      RETURN TRUE;
    ELSIF parent_id = new_id THEN
      RETURN FALSE;
    ELSE
      SELECT event_type, parent INTO parent_event_type_id, grandparent_id FROM event WHERE id = parent_id;
      RETURN check_event(parent_event_type_id, grandparent_id, new_id);
    END IF;
END $$;

CREATE TABLE event(
    id SERIAL PRIMARY KEY,
    name VARCHAR(128) NOT NULL,
    event_type INT NOT NULL,
    parent INT,
    FOREIGN KEY (event_type) REFERENCES event_type(id),
    FOREIGN KEY (parent) REFERENCES event(id),
    CHECK (check_event(event_type, parent, id))
);

-- Person details
CREATE TABLE phone_number(
    person_id INT NOT NULL,
    phone_number VARCHAR(128) NOT NULL,
    owner VARCHAR(128),
    comment VARCHAR(512),
    event_id INT,
    CONSTRAINT pk_phone_number UNIQUE (person_id, phone_number, event_id),
    FOREIGN KEY (person_id) REFERENCES person(id),
    FOREIGN KEY (event_id) REFERENCES event(id)
);

-- Appointment
CREATE TABLE appointment(
    id SERIAL PRIMARY KEY,
    event INT NOT NULL,
    start_date BIGINT NOT NULL,
    end_date BIGINT NOT NULL,
    room INT,
    FOREIGN KEY (event) REFERENCES event(id),
    FOREIGN KEY (room) REFERENCES room(id)
);

-- Participant type
CREATE TABLE participant_type(
    id SERIAL PRIMARY KEY,
    name VARCHAR(32) NOT NULL
);
INSERT INTO participant_type VALUES (1, 'Tutor');
INSERT INTO participant_type VALUES (2, 'Student');

-- Participant
CREATE TABLE participant(
    event INT NOT NULL,
    person INT NOT NULL,
    participant_type INT NOT NULL,
    FOREIGN KEY (event) REFERENCES event(id),
    FOREIGN KEY (person) REFERENCES person(id),
    FOREIGN KEY (participant_type) REFERENCES participant_type(id),
    PRIMARY KEY (event, person)
);

-- School type
CREATE TABLE school_type(
    id SERIAL PRIMARY KEY,
    name VARCHAR(32) NOT NULL
);
INSERT INTO school_type VALUES (1, 'Gymnasium');
INSERT INTO school_type VALUES (2, 'Realschule');
INSERT INTO school_type VALUES (3, 'Mittelschule');

-- School
CREATE TABLE school(
    id SERIAL PRIMARY KEY,
    name VARCHAR(128) NOT NULL,
    school_type INT NOT NULL,
    address_street VARCHAR(128),
    address_housenumber VARCHAR(32),
    address_postcode VARCHAR(32),
    address_place VARCHAR(128),
    official_id VARCHAR(32),
    number_math_teachers INT,
    FOREIGN KEY (school_type) REFERENCES school_type(id)
);

-- Picture
CREATE TABLE picture(
    id SERIAL PRIMARY KEY,
    data BYTEA
);

-- Illness
CREATE TABLE illness(
    id SERIAL PRIMARY KEY,
    name VARCHAR(128) NOT NULL
);

-- Contest
CREATE TABLE contest(
    id SERIAL PRIMARY KEY,
    name VARCHAR(128) NOT NULL
);

CREATE TABLE contest_participant(
    contest INT NOT NULL,
    person INT NOT NULL,
    FOREIGN KEY (contest) REFERENCES contest(id),
    FOREIGN KEY (person) REFERENCES person(id),
    CONSTRAINT pk_contest PRIMARY KEY (contest, person)
)
