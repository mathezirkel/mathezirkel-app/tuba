// Code formatting
addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.4.2")

// Set version from GIT
addSbtPlugin("com.dwijnand" % "sbt-dynver" % "4.1.1")

// Code coverage
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.6.1")

// Publish artifact to GitLab
addSbtPlugin("com.gilcloud" % "sbt-gitlab" % "0.0.6")

// Make build information available in code
addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.10.0")
