# Mathezirkel Database Schema

This file describes the database schema implemented by Tuba. It shall remain in sync with the actual implementation at any time.
Notice that the split in the various types of data structures is arbitrary. Any line with **(DP)** in it contains sensitive information
which might need to be deleted after some amount of time.

## Basic Data Types

* **(DP)** Human (ID, FamilyName, GivenName, BirthDate, Gender, Street, StreetNumber, PostalCode, Place, Miscellaneous, Picture)
* MathCircle (ID, Name, TypeMathCircleID, EventID) _e.g. Korrespondenzzirkel for Mathezirkel 2019_
* Room (ID, Name, PlaceID, Capacity, Comment)
* School (ID, Official_ID, Name, TypeSchoolID, Street, StreetNumber, PostalCode, Place, NumberMathTeachers)
* Event (ID, Name, SchoolYearID, TypeEventID) _e.g. Matheolympiade 2019_
* TimeSlot (ID, Date?, Weekday?, StartTime, EndTime) _can be either recurring (Date is null) or a specific day (Weekday is null)_

## Enums
* SchoolYear (ID, FirstYear, SecondYear) _e.g. 2018/2019, ..._
* Contest (ID, Name) _e.g. Matheolympiade, Känguruwettbewerb, ..._
* Place (ID, Name) _e.g. Violau, Augsburg, ..._
* Topic (ID, Name, Comment) _e.g. Ungleichungen, P=NP, ..._
* Activity (ID, Name, Comment) _e.g. Chor, Programmieren, ..._
* Penalty (ID, Name, Comment) _e.g. Tischdienst, ..._
* Illness (ID, Name, Comment) _e.g. Bienenstichallergie, ..._
* Instrument (ID, Name) _e.g. Querflöte, ...._
* FoodRestriction (ID, Name) _e.g. Vegetarisch, Laktoseintoleranz, ..._  
* Transport (ID, Name) _e.g. Privat, Bus, ..._
* Equipment (ID, Name, Comment) _e.g. Beamer, Dominosteine, ..._
* Prize (ID, Name, Comment) _e.g. Der Zahlenteufel, Jonglierset, ..._

## Type Enums

* TypeHuman (ID, Name) _e.g. BetreuerIn, SchülerIn, ..._
* TypeMathCircle (ID, Name) _e.g. Präsenzzirkel, Korrespondenzzirkel, Campzirkel, ..._
* TypeEvent (ID, Name) _e.g. Mathezirkel, Mai-Mathetag, Matheolympiade, ..._
* TypeSchool (ID, Name) _e.g. Gymnasium, ..._
* TypePrize (ID, Name) _e.g. Buch, Spiel, ..._

## Associations

### General

* ContestParticipants (HumanID, ContestID, SchoolYearID)
* Participants (EventID, HumanID, TypeHumanID, SignedOff) _for all types of events_
* Grades (SchoolYearID, HumanID, Grade)
* **(DP)** PhoneNumbers (HumanID, Number, Owner?, Comment, Emergency) _Emergency is Boolean, Owner is null if number belongs to HumanID_ 
* **(DP)** EmailAddresses (HumanID, EmailAdress, Owner?, Comment) _Owner is null if email adress belongs to HumanID_
* Newsletters (HumanID, SchoolYearID)
* PrizeStock (PrizeID, Count, Place, TypeEventID)
* TopicNeeds (TopicID, EquipmentID)
* FoodRestrictions (EventID, HumanID, FoodRestrictionID, Comment)

### Matheolympiade

* MatholympiadPrizes (EventID, HumanID, Prize) _Prize can be 1, 2, 3 or Anerkennung and only concernes our local math olympiad_
* MatholympiadParticipants (EventID, HumanID, Comment) _some more information?_

### Mathecamp

* Illnesses (EventID, HumanID, IllnessID, Comment)
* Instruments (EventID, HumanID, InstrumentID, Comment)
* Penalties (EventID, HumanID, PenaltyID, DateTime, Comment)
* Arrivals (EventID, HumanID, TransportID, DateTime, Comment)
* **(DP)** Departures (EventID, HumanID, TransportID, DateTime, AllowedPersons, Comment)
* Friends (EventID, HumanID, HumanID)
* Enemies (EventID, HumanID, HumanID)
* **(DP)** FriendWishes (EventID, HumanID, GivenName, FamilyName, Anti, Comment) _when registering, the friend might not be registered yet_
* MathcampParticipants (EventID, HumanID, Campprize, MoneyPayedAlready, RidesharingPermission, SwimmingPermission, LeavingPermission, SportsPermission, Present)
* EquipmentInRooms (RoomID, EquipmentID, Comment) _e.g. Aspachtal has a whiteboard, ..._
